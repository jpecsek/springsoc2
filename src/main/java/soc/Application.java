package soc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;



@SpringBootApplication
public class Application implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(Application.class);	
	
	 public static void main(String[] args) {
	        SpringApplication.run(Application.class, args);
	        System.out.println("Main");
	 }

 
    public void run(String... strings) throws Exception {

    	System.out.println("Run");		
		
	}
}
