package soc;



import java.sql.Clob;
import java.sql.Date;
import java.sql.SQLException;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Classes {
	@Id
	private String CRSE_ID;
	private String SUBJECT;
	private String CATALOG_NBR;
	private Integer UNITS_ACAD_PROG;
	private String DESCRFORMAL;
	private String SUBJECT_DESCR;
	private String TERM_DESCR30;
	private String STRM;
	private String SESSION_CODE;
	private String UC_SESS_CODE_XLAT;
	private String CLASS_SECTION;
	private String COURSE;
	private String UC_EQUIV_CRSE_DESC;
	private String ACAD_CAREER;
	private Integer CLASS_NBR;
	private Integer UC_GAE_CLASS_NBR;
	private String CAMPUS;
	private String LOCATION;
	private String LOCATION_DESCR;
	private String CLASS_STAT;
	private Date START_DT;
	private Date END_DT;	
	private String MON;
	private String TUES;
	private String WED;
	private String THURS;
	private String FRI;
	private String SAT;
	private String SUN;
	private Date MEETING_TIME_START;
	private Date MEETING_TIME_END;
	private String UC_CLASS_STAT_XLAT;
	private String UC_ENRL_STAT_XLAT;
	private Date CANCEL_DT;
	private Integer WAIT_TOT;
	private String BLDG_CD;
	private String UC_BLDG_CD_XLAT;
	private String ROOM;
	private Integer CLASS_NOTES_SEQ;
	private String CLASS_NOTE_NBR;
	private String EMPLID;
	private String COMMON_ID;
	private String INSTR_NAME;
	private String UC_DESCR_A;
	private String INSTR_ROLE;
	private Date REPORT_DATE;
	private String REPORT_TIME;
	private String COURSE_TITLE_LONG;
	private String COUNTRY;
	private String DESCR40;
	private String DESCRLONG;
	private String DESCRLONGCONV;
	private Integer CRSE_OFFER_NBR;
	private String CLASS_NOTE;
	
	
	public Classes(){
		
	}

	
	
	
	
	/**
	 * @return the cRSE_ID
	 */
	public String getCRSE_ID() {
		return CRSE_ID;
	}
	/**
	 * @param cRSE_ID the cRSE_ID to set
	 */
	public void setCRSE_ID(String cRSE_ID) {
		CRSE_ID = cRSE_ID;
	}
	/**
	 * @return the sUBJECT
	 */
	public String getSUBJECT() {
		return SUBJECT;
	}
	/**
	 * @param sUBJECT the sUBJECT to set
	 */
	public void setSUBJECT(String sUBJECT) {
		SUBJECT = sUBJECT;
	}
	/**
	 * @return the cATALOG_NBR
	 */
	public String getCATALOG_NBR() {
		return CATALOG_NBR;
	}
	/**
	 * @param cATALOG_NBR the cATALOG_NBR to set
	 */
	public void setCATALOG_NBR(String cATALOG_NBR) {
		CATALOG_NBR = cATALOG_NBR;
	}
	/**
	 * @return the uNITS_ACAD_PROG
	 */
	public Integer getUNITS_ACAD_PROG() {
		return UNITS_ACAD_PROG;
	}
	/**
	 * @param uNITS_ACAD_PROG the uNITS_ACAD_PROG to set
	 */
	public void setUNITS_ACAD_PROG(Integer uNITS_ACAD_PROG) {
		UNITS_ACAD_PROG = uNITS_ACAD_PROG;
	}
	/**
	 * @return the dESCRFORMAL
	 */
	public String getDESCRFORMAL() {
		return DESCRFORMAL;
	}
	/**
	 * @param dESCRFORMAL the dESCRFORMAL to set
	 */
	public void setDESCRFORMAL(String dESCRFORMAL) {
		DESCRFORMAL = dESCRFORMAL;
	}
	/**
	 * @return the sUBJECT_DESCR
	 */
	public String getSUBJECT_DESCR() {
		return SUBJECT_DESCR;
	}
	/**
	 * @param sUBJECT_DESCR the sUBJECT_DESCR to set
	 */
	public void setSUBJECT_DESCR(String sUBJECT_DESCR) {
		SUBJECT_DESCR = sUBJECT_DESCR;
	}
	/**
	 * @return the tERM_DESCR30
	 */
	public String getTERM_DESCR30() {
		return TERM_DESCR30;
	}
	/**
	 * @param tERM_DESCR30 the tERM_DESCR30 to set
	 */
	public void setTERM_DESCR30(String tERM_DESCR30) {
		TERM_DESCR30 = tERM_DESCR30;
	}
	/**
	 * @return the sTRM
	 */
	public String getSTRM() {
		return STRM;
	}
	/**
	 * @param sTRM the sTRM to set
	 */
	public void setSTRM(String sTRM) {
		STRM = sTRM;
	}
	/**
	 * @return the sESSION_CODE
	 */
	public String getSESSION_CODE() {
		return SESSION_CODE;
	}
	/**
	 * @param sESSION_CODE the sESSION_CODE to set
	 */
	public void setSESSION_CODE(String sESSION_CODE) {
		SESSION_CODE = sESSION_CODE;
	}
	/**
	 * @return the uC_SESS_CODE_XLAT
	 */
	public String getUC_SESS_CODE_XLAT() {
		return UC_SESS_CODE_XLAT;
	}
	/**
	 * @param uC_SESS_CODE_XLAT the uC_SESS_CODE_XLAT to set
	 */
	public void setUC_SESS_CODE_XLAT(String uC_SESS_CODE_XLAT) {
		UC_SESS_CODE_XLAT = uC_SESS_CODE_XLAT;
	}
	/**
	 * @return the cLASS_SECTION
	 */
	public String getCLASS_SECTION() {
		return CLASS_SECTION;
	}
	/**
	 * @param cLASS_SECTION the cLASS_SECTION to set
	 */
	public void setCLASS_SECTION(String cLASS_SECTION) {
		CLASS_SECTION = cLASS_SECTION;
	}
	/**
	 * @return the cOURSE
	 */
	public String getCOURSE() {
		return COURSE;
	}
	/**
	 * @param cOURSE the cOURSE to set
	 */
	public void setCOURSE(String cOURSE) {
		COURSE = cOURSE;
	}
	/**
	 * @return the uC_EQUIV_CRSE_DESC
	 */
	public String getUC_EQUIV_CRSE_DESC() {
		return UC_EQUIV_CRSE_DESC;
	}
	/**
	 * @param uC_EQUIV_CRSE_DESC the uC_EQUIV_CRSE_DESC to set
	 */
	public void setUC_EQUIV_CRSE_DESC(String uC_EQUIV_CRSE_DESC) {
		UC_EQUIV_CRSE_DESC = uC_EQUIV_CRSE_DESC;
	}
	/**
	 * @return the aCAD_CAREER
	 */
	public String getACAD_CAREER() {
		return ACAD_CAREER;
	}
	/**
	 * @param aCAD_CAREER the aCAD_CAREER to set
	 */
	public void setACAD_CAREER(String aCAD_CAREER) {
		ACAD_CAREER = aCAD_CAREER;
	}
	/**
	 * @return the cLASS_NBR
	 */
	public Integer getCLASS_NBR() {
		return CLASS_NBR;
	}
	/**
	 * @param cLASS_NBR the cLASS_NBR to set
	 */
	public void setCLASS_NBR(Integer cLASS_NBR) {
		CLASS_NBR = cLASS_NBR;
	}
	/**
	 * @return the uC_GAE_CLASS_NBR
	 */
	public Integer getUC_GAE_CLASS_NBR() {
		return UC_GAE_CLASS_NBR;
	}
	/**
	 * @param uC_GAE_CLASS_NBR the uC_GAE_CLASS_NBR to set
	 */
	public void setUC_GAE_CLASS_NBR(Integer uC_GAE_CLASS_NBR) {
		UC_GAE_CLASS_NBR = uC_GAE_CLASS_NBR;
	}
	/**
	 * @return the cAMPUS
	 */
	public String getCAMPUS() {
		return CAMPUS;
	}
	/**
	 * @param cAMPUS the cAMPUS to set
	 */
	public void setCAMPUS(String cAMPUS) {
		CAMPUS = cAMPUS;
	}
	/**
	 * @return the lOCATION
	 */
	public String getLOCATION() {
		return LOCATION;
	}
	/**
	 * @param lOCATION the lOCATION to set
	 */
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	/**
	 * @return the lOCATION_DESCR
	 */
	public String getLOCATION_DESCR() {
		return LOCATION_DESCR;
	}
	/**
	 * @param lOCATION_DESCR the lOCATION_DESCR to set
	 */
	public void setLOCATION_DESCR(String lOCATION_DESCR) {
		LOCATION_DESCR = lOCATION_DESCR;
	}
	/**
	 * @return the cLASS_STAT
	 */
	public String getCLASS_STAT() {
		return CLASS_STAT;
	}
	/**
	 * @param cLASS_STAT the cLASS_STAT to set
	 */
	public void setCLASS_STAT(String cLASS_STAT) {
		CLASS_STAT = cLASS_STAT;
	}
	/**
	 * @return the sTART_DT
	 */
	public Date getSTART_DT() {
		return START_DT;
	}
	/**
	 * @param sTART_DT the sTART_DT to set
	 */
	public void setSTART_DT(Date sTART_DT) {
		START_DT = sTART_DT;
	}
	/**
	 * @return the eND_DT
	 */
	public Date getEND_DT() {
		return END_DT;
	}
	/**
	 * @param eND_DT the eND_DT to set
	 */
	public void setEND_DT(Date eND_DT) {
		END_DT = eND_DT;
	}
	/**
	 * @return the mON
	 */
	public String getMON() {
		return MON;
	}
	/**
	 * @param mON the mON to set
	 */
	public void setMON(String mON) {
		MON = mON;
	}
	/**
	 * @return the tUES
	 */
	public String getTUES() {
		return TUES;
	}
	/**
	 * @param tUES the tUES to set
	 */
	public void setTUES(String tUES) {
		TUES = tUES;
	}
	/**
	 * @return the wED
	 */
	public String getWED() {
		return WED;
	}
	/**
	 * @param wED the wED to set
	 */
	public void setWED(String wED) {
		WED = wED;
	}
	/**
	 * @return the tHURS
	 */
	public String getTHURS() {
		return THURS;
	}
	/**
	 * @param tHURS the tHURS to set
	 */
	public void setTHURS(String tHURS) {
		THURS = tHURS;
	}
	/**
	 * @return the fRI
	 */
	public String getFRI() {
		return FRI;
	}
	/**
	 * @param fRI the fRI to set
	 */
	public void setFRI(String fRI) {
		FRI = fRI;
	}
	/**
	 * @return the sAT
	 */
	public String getSAT() {
		return SAT;
	}
	/**
	 * @param sAT the sAT to set
	 */
	public void setSAT(String sAT) {
		SAT = sAT;
	}
	/**
	 * @return the sUN
	 */
	public String getSUN() {
		return SUN;
	}
	/**
	 * @param sUN the sUN to set
	 */
	public void setSUN(String sUN) {
		SUN = sUN;
	}
	/**
	 * @return the mEETING_TIME_START
	 */
	public Date getMEETING_TIME_START() {
		return MEETING_TIME_START;
	}
	/**
	 * @param mEETING_TIME_START the mEETING_TIME_START to set
	 */
	public void setMEETING_TIME_START(Date mEETING_TIME_START) {
		MEETING_TIME_START = mEETING_TIME_START;
	}
	/**
	 * @return the mEETING_TIME_END
	 */
	public Date getMEETING_TIME_END() {
		return MEETING_TIME_END;
	}
	/**
	 * @param mEETING_TIME_END the mEETING_TIME_END to set
	 */
	public void setMEETING_TIME_END(Date mEETING_TIME_END) {
		MEETING_TIME_END = mEETING_TIME_END;
	}
	/**
	 * @return the uC_CLASS_STAT_XLAT
	 */
	public String getUC_CLASS_STAT_XLAT() {
		return UC_CLASS_STAT_XLAT;
	}
	/**
	 * @param uC_CLASS_STAT_XLAT the uC_CLASS_STAT_XLAT to set
	 */
	public void setUC_CLASS_STAT_XLAT(String uC_CLASS_STAT_XLAT) {
		UC_CLASS_STAT_XLAT = uC_CLASS_STAT_XLAT;
	}
	/**
	 * @return the uC_ENRL_STAT_XLAT
	 */
	public String getUC_ENRL_STAT_XLAT() {
		return UC_ENRL_STAT_XLAT;
	}
	/**
	 * @param uC_ENRL_STAT_XLAT the uC_ENRL_STAT_XLAT to set
	 */
	public void setUC_ENRL_STAT_XLAT(String uC_ENRL_STAT_XLAT) {
		UC_ENRL_STAT_XLAT = uC_ENRL_STAT_XLAT;
	}
	/**
	 * @return the cANCEL_DT
	 */
	public Date getCANCEL_DT() {
		return CANCEL_DT;
	}
	/**
	 * @param cANCEL_DT the cANCEL_DT to set
	 */
	public void setCANCEL_DT(Date cANCEL_DT) {
		CANCEL_DT = cANCEL_DT;
	}
	/**
	 * @return the wAIT_TOT
	 */
	public Integer getWAIT_TOT() {
		return WAIT_TOT;
	}
	/**
	 * @param wAIT_TOT the wAIT_TOT to set
	 */
	public void setWAIT_TOT(Integer wAIT_TOT) {
		WAIT_TOT = wAIT_TOT;
	}
	/**
	 * @return the bLDG_CD
	 */
	public String getBLDG_CD() {
		return BLDG_CD;
	}
	/**
	 * @param bLDG_CD the bLDG_CD to set
	 */
	public void setBLDG_CD(String bLDG_CD) {
		BLDG_CD = bLDG_CD;
	}
	/**
	 * @return the uC_BLDG_CD_XLAT
	 */
	public String getUC_BLDG_CD_XLAT() {
		return UC_BLDG_CD_XLAT;
	}
	/**
	 * @param uC_BLDG_CD_XLAT the uC_BLDG_CD_XLAT to set
	 */
	public void setUC_BLDG_CD_XLAT(String uC_BLDG_CD_XLAT) {
		UC_BLDG_CD_XLAT = uC_BLDG_CD_XLAT;
	}
	/**
	 * @return the rOOM
	 */
	public String getROOM() {
		return ROOM;
	}
	/**
	 * @param rOOM the rOOM to set
	 */
	public void setROOM(String rOOM) {
		ROOM = rOOM;
	}
	/**
	 * @return the cLASS_NOTES_SEQ
	 */
	public Integer getCLASS_NOTES_SEQ() {
		return CLASS_NOTES_SEQ;
	}
	/**
	 * @param cLASS_NOTES_SEQ the cLASS_NOTES_SEQ to set
	 */
	public void setCLASS_NOTES_SEQ(Integer cLASS_NOTES_SEQ) {
		CLASS_NOTES_SEQ = cLASS_NOTES_SEQ;
	}
	/**
	 * @return the cLASS_NOTE_NBR
	 */
	public String getCLASS_NOTE_NBR() {
		return CLASS_NOTE_NBR;
	}
	/**
	 * @param cLASS_NOTE_NBR the cLASS_NOTE_NBR to set
	 */
	public void setCLASS_NOTE_NBR(String cLASS_NOTE_NBR) {
		CLASS_NOTE_NBR = cLASS_NOTE_NBR;
	}
	/**
	 * @return the eMPLID
	 */
	public String getEMPLID() {
		return EMPLID;
	}
	/**
	 * @param eMPLID the eMPLID to set
	 */
	public void setEMPLID(String eMPLID) {
		EMPLID = eMPLID;
	}
	/**
	 * @return the cOMMON_ID
	 */
	public String getCOMMON_ID() {
		return COMMON_ID;
	}
	/**
	 * @param cOMMON_ID the cOMMON_ID to set
	 */
	public void setCOMMON_ID(String cOMMON_ID) {
		COMMON_ID = cOMMON_ID;
	}
	/**
	 * @return the iNSTR_NAME
	 */
	public String getINSTR_NAME() {
		return INSTR_NAME;
	}
	/**
	 * @param iNSTR_NAME the iNSTR_NAME to set
	 */
	public void setINSTR_NAME(String iNSTR_NAME) {
		INSTR_NAME = iNSTR_NAME;
	}
	/**
	 * @return the uC_DESCR_A
	 */
	public String getUC_DESCR_A() {
		return UC_DESCR_A;
	}
	/**
	 * @param uC_DESCR_A the uC_DESCR_A to set
	 */
	public void setUC_DESCR_A(String uC_DESCR_A) {
		UC_DESCR_A = uC_DESCR_A;
	}
	/**
	 * @return the iNSTR_ROLE
	 */
	public String getINSTR_ROLE() {
		return INSTR_ROLE;
	}
	/**
	 * @param iNSTR_ROLE the iNSTR_ROLE to set
	 */
	public void setINSTR_ROLE(String iNSTR_ROLE) {
		INSTR_ROLE = iNSTR_ROLE;
	}
	/**
	 * @return the rEPORT_DATE
	 */
	public Date getREPORT_DATE() {
		return REPORT_DATE;
	}
	/**
	 * @param rEPORT_DATE the rEPORT_DATE to set
	 */
	public void setREPORT_DATE(Date rEPORT_DATE) {
		REPORT_DATE = rEPORT_DATE;
	}
	/**
	 * @return the rEPORT_TIME
	 */
	public String getREPORT_TIME() {
		return REPORT_TIME;
	}
	/**
	 * @param rEPORT_TIME the rEPORT_TIME to set
	 */
	public void setREPORT_TIME(String rEPORT_TIME) {
		REPORT_TIME = rEPORT_TIME;
	}
	/**
	 * @return the cOURSE_TITLE_LONG
	 */
	public String getCOURSE_TITLE_LONG() {
		return COURSE_TITLE_LONG;
	}
	/**
	 * @param cOURSE_TITLE_LONG the cOURSE_TITLE_LONG to set
	 */
	public void setCOURSE_TITLE_LONG(String cOURSE_TITLE_LONG) {
		COURSE_TITLE_LONG = cOURSE_TITLE_LONG;
	}
	/**
	 * @return the cOUNTRY
	 */
	public String getCOUNTRY() {
		return COUNTRY;
	}
	/**
	 * @param cOUNTRY the cOUNTRY to set
	 */
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	/**
	 * @return the dESCR40
	 */
	public String getDESCR40() {
		return DESCR40;
	}
	/**
	 * @param dESCR40 the dESCR40 to set
	 */
	public void setDESCR40(String dESCR40) {
		DESCR40 = dESCR40;
	}
	
	
	
	/**
	 * @return the dESCRLONG
	 */
	public String getDESCRLONG() {
		return DESCRLONG;
	}
	/**
	 * @param string the dESCRLONG to set
	 */
	public void setDESCRLONG(String string) {
				
		DESCRLONG = string;
	}
	
	/**
	 * 
	 * @param desc
	 * @return
	 */
	//Replaced by lobHandler
	/*public String convertClob(Clob desc) {
		String x = null;
		if (desc != null){
			try {
				x = desc.getSubString(1, (int) desc.length());
				System.out.println("Clob success" + x);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Clob fail");
			}
		}
		return x;
	}*/
	/**
	 * 
	 * @param dESCRLONGCONV
	 */
	public void setDESCRLONGCONV(String dESCRLONGCONV) {
		
		DESCRLONGCONV = dESCRLONGCONV;
	}
	
	
	
	/**
	 * @return the cRSE_OFFER_NBR
	 */
	public Integer getCRSE_OFFER_NBR() {
		return CRSE_OFFER_NBR;
	}
	/**
	 * @param cRSE_OFFER_NBR the cRSE_OFFER_NBR to set
	 */
	public void setCRSE_OFFER_NBR(Integer cRSE_OFFER_NBR) {
		CRSE_OFFER_NBR = cRSE_OFFER_NBR;
	}
	/**
	 * @return the cLASS_NOTE
	 */
	public String getCLASS_NOTE() {
		return CLASS_NOTE;
	}
	/**
	 * @param cLASS_NOTE the cLASS_NOTE to set
	 */
	public void setCLASS_NOTE(String cLASS_NOTE) {
		CLASS_NOTE = cLASS_NOTE;
	}
	

}