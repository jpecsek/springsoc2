package soc;


import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale.Category;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.repository.PagingAndSortingRepository;

@RestController
public class ClassController {
	
    //Instructions at: https://spring.io/guides/gs/rest-service/#initial
    //http://localhost:8080/class	
	
	private static final String SQL_LIST ="SELECT * FROM (SELECT ACAD_CAREER,BLDG_CD,CATALOG_NBR,CLASS_NBR,S.CLASS_SECTION,CLASS_STAT,COURSE_TITLE_LONG,S.CRSE_ID AS CLASS_NOTE,END_DT,FRI,INSTR_NAME,LOCATION,LOCATION_DESCR,MEETING_TIME_END,MEETING_TIME_START,MON,ROOM,SAT,SESSION_CODE,START_DT,S.STRM,SUBJECT,SUBJECT_DESCR,SUN,TERM_DESCR30,THURS,TUES,UC_BLDG_CD_XLAT,UC_ENRL_STAT_XLAT,UC_SESS_CODE_XLAT,UNITS_ACAD_PROG,WED,ROW_NUMBER() OVER( ORDER BY SUBJECT ASC, CATALOG_NBR ASC, CLASS_NBR, S.CLASS_SECTION, START_DT, MEETING_TIME_START ) R   FROM PS_UCW_SR201_VW S   LEFT JOIN PS_UCW_SR201_CN N   ON S.CRSE_ID = N.CRSE_ID   AND S.CLASS_SECTION = N.CLASS_SECTION   AND S.STRM = N.STRM   WHERE ACAD_CAREER = 'UGRD'   AND S.STRM = '2158'   ) TheList WHERE R BETWEEN 1 AND 100";
    
	private static final String SQL_LIST2 ="SELECT ACAD_CAREER,BLDG_CD,CATALOG_NBR FROM  PS_UCW_SR201_VW";
    
	private static final String SQL_LIST3 ="SELECT * FROM (SELECT ACAD_CAREER,BLDG_CD,CATALOG_NBR,CLASS_NBR,S.CLASS_SECTION,CLASS_STAT,COURSE_TITLE_LONG,S.CRSE_ID,S.DESCRLONG,N.DESCRLONG AS CLASS_NOTE,END_DT,FRI,INSTR_NAME,LOCATION,LOCATION_DESCR,MEETING_TIME_END,MEETING_TIME_START,MON,ROOM,SAT,SESSION_CODE,START_DT,S.STRM,SUBJECT,SUBJECT_DESCR,SUN,TERM_DESCR30,THURS,TUES,UC_BLDG_CD_XLAT,UC_ENRL_STAT_XLAT,UC_SESS_CODE_XLAT,UNITS_ACAD_PROG,WED,ROW_NUMBER() OVER( ORDER BY SUBJECT ASC, CATALOG_NBR ASC, CLASS_NBR, S.CLASS_SECTION, START_DT, MEETING_TIME_START ) R   FROM PS_UCW_SR201_VW S   LEFT JOIN PS_UCW_SR201_CN N   ON S.CRSE_ID = N.CRSE_ID   AND S.CLASS_SECTION = N.CLASS_SECTION   AND S.STRM = N.STRM   WHERE ACAD_CAREER = 'UGRD'   AND S.STRM = '2158'   ) TheList WHERE R BETWEEN 1 AND 100";
	
	@Autowired
    JdbcTemplate jdbcTemplate;	
	
	LobHandler lobHandler = new DefaultLobHandler();
	
	/**
	 * Returns integer of record count
	 * @return
	 */
    @RequestMapping("/countInteger")
    public Integer count() {
    	Integer rowCount = this.jdbcTemplate.queryForObject("select count(*) from PS_UCW_SR201_VW", Integer.class);
    	System.out.println(rowCount);
        return rowCount;
    }
    
    /**
     * Returns json string with record count
     * @return
     */
    @RequestMapping("/countObject")
    public Class countObject() {
    	Integer rowCount = this.jdbcTemplate.queryForObject("select count(*) from PS_UCW_SR201_VW", Integer.class);
        return new Class(rowCount);
    }
    
    @RequestMapping("/coursemap")
    public List<Classes> getCourseold() {
    	
    	List<Classes> coursemap = this.jdbcTemplate.query(SQL_LIST3,
    	
		
        new RowMapper<Classes>() {
            public Classes mapRow(ResultSet rs, int rowNum) throws SQLException {		
    			
		    	Classes classes = new Classes();
		    	
		    	classes.setACAD_CAREER(rs.getString("ACAD_CAREER"));
		    	classes.setBLDG_CD(rs.getString("BLDG_CD"));
		    	classes.setCATALOG_NBR(rs.getString("CATALOG_NBR"));
		    	
		    	classes.setCLASS_SECTION(rs.getString("cLASS_SECTION"));
                classes.setCLASS_STAT(rs.getString("cLASS_STAT"));
                classes.setCOURSE_TITLE_LONG(rs.getString("cOURSE_TITLE_LONG"));
                classes.setCRSE_ID(rs.getString("cRSE_ID"));
                classes.setEND_DT(rs.getDate("eND_DT"));
                
                classes.setMON(rs.getString("mON"));
                classes.setTUES(rs.getString("tUES"));
                classes.setWED(rs.getString("wED"));
                classes.setTHURS(rs.getString("tHURS"));
                classes.setFRI(rs.getString("fRI"));
                classes.setSAT(rs.getString("sAT"));
                classes.setSUN(rs.getString("sUN"));
                
                classes.setINSTR_NAME(rs.getString("iNSTR_NAME"));
                classes.setLOCATION(rs.getString("lOCATION"));
                classes.setLOCATION_DESCR(rs.getString("lOCATION_DESCR"));
                classes.setMEETING_TIME_START(rs.getDate("mEETING_TIME_START"));
                classes.setMEETING_TIME_END(rs.getDate("mEETING_TIME_END"));
                classes.setROOM(rs.getString("rOOM"));
                classes.setSESSION_CODE(rs.getString("sESSION_CODE"));
                classes.setSTART_DT(rs.getDate("sTART_DT"));
                classes.setSTRM(rs.getString("sTRM"));
                classes.setSUBJECT(rs.getString("sUBJECT"));
                classes.setSUBJECT_DESCR(rs.getString("sUBJECT_DESCR"));
                classes.setTERM_DESCR30(rs.getString("tERM_DESCR30"));
                classes.setUC_BLDG_CD_XLAT(rs.getString("uC_BLDG_CD_XLAT"));
                classes.setUC_ENRL_STAT_XLAT(rs.getString("uC_ENRL_STAT_XLAT"));
                classes.setUC_SESS_CODE_XLAT(rs.getString("uC_SESS_CODE_XLAT"));
                classes.setUNITS_ACAD_PROG(rs.getInt("uNITS_ACAD_PROG"));
		    	
		    	
                classes.setDESCRLONG(lobHandler.getClobAsString(rs, "dESCRLONG"));		    	
		    	
		        return classes;
            }
            	 
    	});
		return coursemap;
    }
     
    
    
    @RequestMapping("/coursebean")
    public List<Classes> getCourse() {
		
    	List<Classes> courses  = this.jdbcTemplate.query(SQL_LIST,
    			new BeanPropertyRowMapper(Classes.class));
    	return courses;
    }
    
  
    @RequestMapping("/coursemappag")
    public List<Classes> getCoursePag(@RequestParam(value="", defaultValue="0") String page) {
    	
    	
    	
    	Integer rowCount = this.jdbcTemplate.queryForObject("select count(*) from PS_UCW_SR201_VW", Integer.class);
        //return new Class(rowCount);
    	
    	
    	List<Classes> coursemap = this.jdbcTemplate.query(SQL_LIST3,
    			
        new RowMapper<Classes>() {
            public Classes mapRow(ResultSet rs, int rowNum) throws SQLException {		
    			
		    	Classes classes = new Classes();
		    	
		    	classes.setACAD_CAREER(rs.getString("ACAD_CAREER"));
		    	classes.setBLDG_CD(rs.getString("BLDG_CD"));
		    	classes.setCATALOG_NBR(rs.getString("CATALOG_NBR"));
		    	
		    	classes.setCLASS_SECTION(rs.getString("cLASS_SECTION"));
                classes.setCLASS_STAT(rs.getString("cLASS_STAT"));
                classes.setCOURSE_TITLE_LONG(rs.getString("cOURSE_TITLE_LONG"));
                classes.setCRSE_ID(rs.getString("cRSE_ID"));
                classes.setEND_DT(rs.getDate("eND_DT"));
                
                classes.setMON(rs.getString("mON"));
                classes.setTUES(rs.getString("tUES"));
                classes.setWED(rs.getString("wED"));
                classes.setTHURS(rs.getString("tHURS"));
                classes.setFRI(rs.getString("fRI"));
                classes.setSAT(rs.getString("sAT"));
                classes.setSUN(rs.getString("sUN"));
                
                classes.setINSTR_NAME(rs.getString("iNSTR_NAME"));
                classes.setLOCATION(rs.getString("lOCATION"));
                classes.setLOCATION_DESCR(rs.getString("lOCATION_DESCR"));
                classes.setMEETING_TIME_START(rs.getDate("mEETING_TIME_START"));
                classes.setMEETING_TIME_END(rs.getDate("mEETING_TIME_END"));
                classes.setROOM(rs.getString("rOOM"));
                classes.setSESSION_CODE(rs.getString("sESSION_CODE"));
                classes.setSTART_DT(rs.getDate("sTART_DT"));
                classes.setSTRM(rs.getString("sTRM"));
                classes.setSUBJECT(rs.getString("sUBJECT"));
                classes.setSUBJECT_DESCR(rs.getString("sUBJECT_DESCR"));
                classes.setTERM_DESCR30(rs.getString("tERM_DESCR30"));
                classes.setUC_BLDG_CD_XLAT(rs.getString("uC_BLDG_CD_XLAT"));
                classes.setUC_ENRL_STAT_XLAT(rs.getString("uC_ENRL_STAT_XLAT"));
                classes.setUC_SESS_CODE_XLAT(rs.getString("uC_SESS_CODE_XLAT"));
                classes.setUNITS_ACAD_PROG(rs.getInt("uNITS_ACAD_PROG"));
		    	
		    	
                classes.setDESCRLONG(lobHandler.getClobAsString(rs, "dESCRLONG"));		    	
		    	
		        return classes;
            }
            	 
    	});
		return coursemap;
    }	
    	
    
}
